Clé anti contact 

Utile pour :

 * Eviter de toucher les portes quand il faut les tirer
 * Toucher les machines à café et les fontaines à eau (double bouton mais attention à ne pas vous bruler)

Fichiers fournis :
 * [stl](https://gitlab.com/pastel-fablab/projets/covid-tools/-/blob/master/covid-key/CovidKey_v5.stl)
 * [Source Fusion 360](https://gitlab.com/pastel-fablab/projets/covid-tools/-/blob/master/covid-key/CovidKey_v5.f3d)

Source sur thingiverse : https://www.thingiverse.com/thing:4605540

Fork d'une [version](https://www.thingiverse.com/thing:4272351) de thingiverse, à laquelle est rajoutée 2 ergots.


![alt exemple ouverture de porte](20200928_113508.jpg)
![alt accroche sur le tour de cou](20200928_113431.jpg)



 

 
